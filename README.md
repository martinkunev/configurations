# Configurations

Useful configuration files I use on my computer

* /etc/bc

  check https://martinkunev.wordpress.com/2022/06/01/bc-the-command-line-calculator/
* /etc/vim/vimrc.local

## Linux-specific (using Debian/Devuan Linux)

* /etc/bashrc_local
* /etc/asound.conf

  check https://martinkunev.wordpress.com/2021/08/11/alsa-with-debian/
